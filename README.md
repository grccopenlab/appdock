AppDock is a C# WPF application used to provide a convenient and consistent interface for accessing he various applications and websites utilized in the Open Computer Lab at GRCC.

AppDock currently generates a dynamic list of actionable buttons from data pulled from an Access database.